﻿// Arthur
// 12 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#endregion

public class PersistentCanvasManager : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private float m_loseStamina = 0f;
    [ SerializeField ] private float m_gainStamina = 0f;

    [ SerializeField ] private Slider m_staminaBar = null;

    public TextMeshProUGUI m_textWoods = null;
    [ SerializeField ] private int m_woodQuantity = 0;
   
    [ SerializeField ] private Transform m_panelInteract = null;

    #endregion

    #region Properties

    public int MWoodQuantity { get { return m_woodQuantity; } }

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        m_panelInteract.gameObject.SetActive ( false );
        UpdateText ( );
    }

    private void Update ( )
    {
        if ( Input.GetKey ( KeyCode.LeftShift ) && ( Input.GetKey ( KeyCode.W ) || Input.GetKey ( KeyCode.S ) || Input.GetKey ( KeyCode.A ) || Input.GetKey ( KeyCode.D ) ) )
        {
            m_staminaBar.value -= m_loseStamina;
        }
        else if ( m_staminaBar.value < m_staminaBar.maxValue )
        {
            if ( Input.GetKey ( KeyCode.W ) || Input.GetKey ( KeyCode.S ) || Input.GetKey ( KeyCode.A ) || Input.GetKey ( KeyCode.D ) )
            {
                m_staminaBar.value += m_gainStamina / 2;
            }
            else
            {
                m_staminaBar.value += m_gainStamina;
            }
        }

        if ( Input.GetKey ( KeyCode.U ) )
        {
            m_woodQuantity += 1;
            UpdateText ( );
        }
    }

    #endregion

    private void UpdateText ( )
    {
        m_textWoods.text = "Wood : " + m_woodQuantity.ToString ( );
    }

    #region Wood Quantity

    public void LoseWood ( int _quantity )
    {
        m_woodQuantity -= _quantity;
        UpdateText ( );
    }

    public void GainWood ( )
    {
        m_woodQuantity++;
        UpdateText ( );
    }

    #endregion

    #region Panel Interact

    public void DisplayPanelInteract ( )
    {
        m_panelInteract.gameObject.SetActive ( true );
    }

    public void HidePanelInteract ( )
    {
        m_panelInteract.gameObject.SetActive ( false );
    }

    public bool ActiveSelfPanelInteract ( )
    {
        return m_panelInteract.gameObject.activeSelf;
    }

    #endregion
}
