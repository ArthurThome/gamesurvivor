﻿// Arthur Martins Thome
// 13 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class Axe : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Animator m_animator = null;

    [ SerializeField ] private LayerMask m_layerTree = new LayerMask ( );
    private Tree m_referenceTree = null;

    [ SerializeField ] private FirstPersonController m_referenceFirstPerson = null;

    #endregion

    private void Update ( )
    {
        if ( Input.GetMouseButtonDown ( 0 ) && m_referenceFirstPerson.GetSpeedMovement ( ) <= 0 )
        {
            AnimAtack ( );
        }
    }

    public void AnimWalk ( float _value, float _speed )
    {
        m_animator.SetFloat ( "WalkSpeed", _value );
        m_animator.speed = _speed;
    }

    public void AnimAtack ( )
    {
        m_animator.SetTrigger ( "AtackAxe" );
    }

    public void HitTree ( )
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay ( Input.mousePosition );

        if ( Physics.Raycast ( ray, out hit, 1, m_layerTree ) )
        {
            m_referenceTree = hit.collider.GetComponent<Tree> ( );

            if ( m_referenceTree != null ) m_referenceTree.HitTree ( );
        }
    }
}


