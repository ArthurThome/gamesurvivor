﻿// Arthur Martins Thome
// 13 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class Tree : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private int m_treeHP = 0;
    [ SerializeField ] private int m_gain = 0;
    [ SerializeField ] private int m_maxDrop = 0;
    [ SerializeField ] private int m_minDrop = 0;

    [ SerializeField ] private Rigidbody m_treeRigidbody = null;
    [ SerializeField ] private GameObject m_woodPrefab = null;

    private Coroutine m_breakCoroutine = null;

    #endregion
    
    void Update ( )
    {
        CheckLifeTree ( );
    }

    private IEnumerator BreakTree ( )
    {
        yield return new WaitForSeconds ( 5.5f );
        m_gain = Random.Range ( m_minDrop, m_maxDrop );

        InstantiateWoodLog ( );
    }

    private void InstantiateWoodLog ( )
    {
        for ( int i = 0; i < m_gain; i++ )
        {
            Instantiate ( m_woodPrefab, new Vector3 ( i * 1f, 0, 0 ) + transform.position, Quaternion.Euler ( 90f, 0f, 0f ) );
        }

        Destroy ( gameObject );
    }

    public void HitTree ( )
    {
        if ( m_treeHP > 0 )
            m_treeHP--;

        CheckLifeTree ( );        
    }

    private void CheckLifeTree ( )
    {
        if ( m_treeHP <= 0 )
        {
            m_treeRigidbody.isKinematic = false;

            m_treeRigidbody.AddForce ( transform.forward );

            if ( m_breakCoroutine == null )
                m_breakCoroutine = StartCoroutine ( BreakTree ( ) );
        }
    }
}
