﻿// Arthur Martins Thome
// 13 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class WeaponSystem : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Transform m_axetransform = null;
    [ SerializeField ] private Transform m_hammertransform = null;
    
    #endregion

    #region Properties

    public bool MCanBuild { get { return m_hammertransform.gameObject.activeSelf; } } 

    #endregion

    #region MonoBehavior

    private void Start ( )
    {
        HideWeapon ( m_hammertransform );
        HideWeapon ( m_axetransform );
    }

    void Update ( )
    {
        if ( Input.GetKeyDown ( KeyCode.Alpha1 ) )
        {
            //instantiate sword
        }

        if ( Input.GetKeyDown ( KeyCode.Alpha2 ) )
        {
            //instantiate Axe
            DisplayWeapon ( m_axetransform );
            HideWeapon ( m_hammertransform );
        }

        if ( Input.GetKeyDown ( KeyCode.Alpha3 ) )
        {
            //instantiate Hammer
            DisplayWeapon ( m_hammertransform );
            HideWeapon ( m_axetransform );
        }

        if ( Input.GetKeyDown ( KeyCode.Alpha4 ) )
        {
            //empty hand
        }
    }

    #endregion

    private void HideWeapon ( Transform _weapon )
    {
        _weapon.gameObject.SetActive ( false );
    }

    private void DisplayWeapon ( Transform _weapon )
    {
        _weapon.gameObject.SetActive ( true );
    }
}
