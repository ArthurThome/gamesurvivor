﻿// Arthur Martins Thome
// 13 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class Forest : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Terrain m_terrain = null;

    [ SerializeField ] private int m_treeQuantity = 0;
    [ SerializeField ] private Tree m_treeprefab = null;

    private int m_currentTreeQuantity = 0;
    private float m_sizeZ = 0;
    private float m_sizeX = 0;
    private float m_posZ = 0;
    private float m_posX = 0;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        m_sizeX = m_terrain.terrainData.size.x;
        m_sizeZ = m_terrain.terrainData.size.z;

        m_posX = m_terrain.transform.position.x;
        m_posZ = m_terrain.transform.position.z;

        while ( m_currentTreeQuantity <= m_treeQuantity )
        {
            float _pos_x = Random.Range ( m_posX, m_posX + m_sizeX );
            float _pos_z = Random.Range ( m_posZ, m_posZ + m_sizeZ );

            float _pos_y = m_terrain.SampleHeight ( new Vector3 ( _pos_x, 0, _pos_z ) );

            Instantiate ( m_treeprefab, new Vector3 ( _pos_x, _pos_y, _pos_z ), Quaternion.identity );

            m_currentTreeQuantity++;
        }
    }

    #endregion
}
