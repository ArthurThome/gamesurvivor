﻿// Arthur Martins Thome
// 12 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class Building : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private PersistentCanvasManager m_referencePersistentCanvas = null;
    [ SerializeField ] private WeaponSystem m_referenceWeaponSystem = null;
       
    [ SerializeField ] private Transform m_cubePreview = null;
    [ SerializeField ] private Transform m_wallPrefab = null;
    [ SerializeField ] private Transform m_doorPrefab = null;
    private Transform m_prefabSelected = null;
    [ SerializeField ] private bool m_canBuild;

    public Material m_materialPreview = null;

    [ SerializeField ] private int m_quantityToBuildDoor = 2;
    [ SerializeField ] private int m_quantityToBuildWall = 1;
       

    #endregion

    #region MonoBehavior Methods
    
    private void Update ( )
    {
        if ( !m_referenceWeaponSystem.MCanBuild ) return;

        RaycastHit hit;
        Vector3 euler = transform.rotation.eulerAngles;
        Ray ray = Camera.main.ScreenPointToRay ( Input.mousePosition );

        if ( Input.GetKeyDown ( KeyCode.Alpha8 ) && m_referencePersistentCanvas.MWoodQuantity > m_quantityToBuildWall )
        {

            m_canBuild = true;
            m_prefabSelected = m_wallPrefab;
            m_cubePreview.transform.localScale = new Vector3 ( m_wallPrefab.transform.localScale.x, m_wallPrefab.transform.localScale.y, m_wallPrefab.transform.localScale.z );
        }

        if ( Input.GetKeyDown ( KeyCode.Alpha9 ) && m_referencePersistentCanvas.MWoodQuantity > m_quantityToBuildDoor )
        {
            m_canBuild = true;
            m_prefabSelected = m_doorPrefab;
            m_cubePreview.transform.localScale = new Vector3 ( m_doorPrefab.transform.localScale.x, m_doorPrefab.transform.localScale.y, m_doorPrefab.transform.localScale.z );
        }

        if ( Input.GetKeyDown ( KeyCode.Alpha0 ) && m_referencePersistentCanvas.MWoodQuantity > 0 )
        {
            m_canBuild = false;
        }

        if ( m_referencePersistentCanvas.MWoodQuantity == 0 )
        {
            m_canBuild = false;
        }

        if ( m_canBuild && Physics.Raycast ( ray, out hit, 15 ) )
        {
            if ( hit.distance > 2 )
            {
                if ( Input.GetKeyDown ( KeyCode.Q ) )
                    transform.Rotate ( 0f, 90f, 0f );

                if ( Input.GetKeyDown ( KeyCode.E ) )
                    transform.Rotate ( 0f, -90f, 0f );

                if ( hit.collider.tag == "Terrain" )
                {
                    m_materialPreview.color = new Color ( 0, 1, 0, 0.5f );
                    transform.position = hit.point + new Vector3 ( 0f, 2f, 0f );

                    if ( Input.GetMouseButtonDown ( 0 ) )
                    {

                        if ( m_prefabSelected == m_doorPrefab )
                        {
                            m_referencePersistentCanvas.LoseWood ( m_quantityToBuildDoor );
                        }
                        else if ( m_prefabSelected == m_wallPrefab )
                        {
                            m_referencePersistentCanvas.LoseWood ( m_quantityToBuildWall );
                        }

                        Instantiate ( m_prefabSelected, transform.position, Quaternion.Euler ( 0, euler.y, 0 ) );

                    }       
                }
            }
            else
            {
                m_cubePreview.gameObject.GetComponent<Renderer> ( ).sharedMaterial.color = new Color ( 1, 0, 0, 0.5f );
            }
        }
        else
        {
            m_cubePreview.gameObject.GetComponent<Renderer> ( ).sharedMaterial.color = new Color ( 1, 0, 0, 0.5f );
            transform.position = new Vector3 ( 0f, -50f, 0f );
        }

        //if ( !m_canBuild ) gameObject.SetActive ( false );
        //else gameObject.SetActive ( true );

        

    }

    #endregion
}
