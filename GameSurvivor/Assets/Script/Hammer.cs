﻿// Arthur Martins Thome
// 13 SEP 2019

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class Hammer : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Animator m_animator = null;

    [ SerializeField ] private LayerMask m_layerTree = new LayerMask ( );
    private Tree m_referenceTree = null;

    [ SerializeField ] private FirstPersonController m_referenceFirstPerson = null;

    #endregion


    private void Update ( )
    {
        if ( Input.GetMouseButton ( 0 ) && m_referenceFirstPerson.GetSpeedMovement ( ) <= 0 )
        {
            AnimAtack ( );
        }
    }

    public void AnimWalk ( float _value, float _speed )
    {
        m_animator.SetFloat ( "WalkSpeed", _value );
        m_animator.speed = _speed;
    }

    public void AnimAtack ( )
    {
        m_animator.SetTrigger ( "AtackHammer" );
    }

    public void Building ( )
    {

    }
}
